# Nextcloud API

- [Nextcloud API](#nextcloud-api)
  - [Функционал утилиты](#функционал-утилиты)
  - [Установка](#установка)
  - [Порядок работы с утилитой](#порядок-работы-с-утилитой)
    - [Конфигурационный файл](#конфигурационный-файл)
    - [Команды](#команды)
  - [GUI](#gui)
    - [Основное окно](#основное-окно)
    - [Окно статуса (Log)](#окно-статуса-log)


Утилита командной строки для синхронизации локальных файлов с NextCloud.

## Функционал утилиты

 - получение информации об имеющихся файлах в NextCloud;
 - Отображение структуры файлов;
 - Загрузка файлов с NextCloud;
 - Выгрузка файлов на NextCloud;
 - Кэширование метаданных о файлах на NextCloud;
 - GUI для всего выше перечисленного.

## Установка

```
pip3 install oscar_nc
```

## Порядок работы с утилитой

### Конфигурационный файл
Для авторизации, указания путей до корневых директорий локально и на сервере используется json конфиг, имеющий следующую структуру:

```json
{
    "login": "login",
    "password": "password",
    "web_address": "https://**your_nextcloud_server_address**",
    "remote_folder": "./",
    "local_folder": "./local_data",
    "use_cache": true,
    "metadata_cache_file": "response_cache.json"
}
```

Так как запрос к серверу может занимать до минуты, каждый раз он кэшируется локально в файл, указанный в поле ```metadata_cache_file```.

При запуске утилиты нет необходимости передавать параметром путь до конфиг файла. Вместо этого надо один раз подать его параметром для команды:

```
oscar_nc set_config SOME_PATH/CONFIG_NAME.json
```

Введенный путь будет сохранен в текстовый файл ```~/.oscar_nc/config_path.txt``` и будет автоматически подгружаться при использованиии утилиты.

### Команды

 - ```oscar_nc set_config PATH``` - Задает путь до конфиг файла для последующих команд;
 - ```oscar_nc get_config``` - Вывод текущего пути до конфиг файла и его содержимое;
 - ```oscar_nc list``` - Отображение синхронизованной структуры файлов;
 - ```oscar_nc update``` - Обновление информации о метаданных с сервера;
 - ```oscar_nc help``` - Вывод документации на команды;
 - ```oscar_nc download PATH``` - скачивание содержимого, лежащего по внутри PATH;
 - ```oscar_nc upload PATH``` - Выгрузка файла/папки на сервер;
 - ```oscar_nc_gui``` - запуск GUI для вышеперечисленных действий.


## GUI
Для набиолее удобного использования и отсутствия необходимости вводить все пути до файлов и команды вручную, был разработан GUI. 

![Alt text](docs/main_page.png)

По нажатию клавиши ```h``` открывается окно с документацией GUI.

### Основное окно

В основном окне отображается раскрываемое дерево файлов с информацией о размерах файлов и времени последнего изменения. Под списком файлов отображется используемое место на диске/на сервере. Правее - основные клавиши управления.

Подчеркиванием выделяется выбранный файл. Раскрытие и сворачивание папки осуществляется нажатием ENTER. 

Загрузка / выгрузка выбранного пути объединена в команду synchronize, для которой следует нажать ```s```. Сначала утилита попытается выгрузить имеющиеся локально файлы на сервер, если они там отсутствуют. После - скачает недостающие файлы с сервера. После операций не происходит автоматического обновления статуса файлов из-за возможного долгого ожидания ответа от сервера.

Обновление статуса файлов осуществляется клавишей ```u``` (update). Утилита запрашивает метаданные с сервера и обновляет информацию о локальных файлах. 

### Окно статуса (Log)

В нижнем окне располагается вывод stdout, отображающий результаты выполнения команд (загрузка/выгрузка файлов/обновление метаданных). 
