_complete() {
    
    if [ "${#COMP_WORDS[@]}" == "2" ]; then
        COMPREPLY=($(compgen -W "update list upload download set_config get_config" -- "${COMP_WORDS[1]}"))
    fi
    if [ "${#COMP_WORDS[@]}" == "3" ] || [ "${COMP_WORDS[2]}" == "upload" ] || [ "${COMP_WORDS[2]}" == "download" ] ; then
        completion_str=$(oscar_nc complete ${COMP_WORDS[2]})
        
        comp_words=()
        j=0
        eval "COMPREPLY=($completion_str)"
    fi
}


complete -F _complete oscar_nc
