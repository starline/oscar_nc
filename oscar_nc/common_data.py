###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

CLI_HELP_STR = '''
OSCAR NextCloud API for synchronizing data with remote storage.

Usage: oscar_nc [ACTION] [[PATH]] [[OPTIONS]]

Actions:
    download [PATH] - download folder/file from remote;
    upload [PATH] - upload folder/file to remote;
    set_config [PATH] - set path to config file;
    list [[--show_files]] - show synchronized file tree with remote. If no cache saved, at first, runs update action;
        --show_files - exposes files in tree. By default, only folders are shown;
    update - download meta data from remote to local cache;
    get_config - get current config;
    help - show this help
    
List action colors:
    GREEN - folder/file synchronized
    CYAN - path exists locally but not remotely
    RED - path exists remotely but not locally

'''


### nc_client constants ###
PROPFIND_REQUEST = '''<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
  <d:prop>
        <d:displayname/>
        <d:getlastmodified />
        <d:getetag />
        <d:getcontenttype />
        <d:resourcetype />
        <oc:fileid />
        <oc:permissions />
        <oc:size />
        <d:quota-used-bytes />
        <d:getcontentlength />
        <nc:has-preview />
        <oc:favorite />
        <oc:comments-unread />
        <oc:owner-display-name />
        <oc:share-types />
        <nc:contained-folder-count />
        <nc:contained-file-count />
  </d:prop>
</d:propfind>
'''
SHOW_SIZE_PROGRESS_THRESHOLD_MB = 500
READ_CHUNK_SIZE = 64 * 1024
DOWNLOAD_CHUNK_SIZE = 1024**2 * 100  # 100 MB



### nc_synchronizer constants ###
FILE_PATH_DEFAULT_PREFIX = "/remote.php/dav/files"

### utils constants ###

ONLY_REMOTE_COLOR = 'red'
ONLY_LOCAL_COLOR = 'cyan'
EVERYWHERE_COLOR = 'green'
TAB = "  "


### GUI constants ###

GUI_HELP_STR = '''
HotKeys:
 u -- update info about files
 s -- synchronize files under path: upload and download if necessary
 h -- show this help
 
 PgUp -- page up in tree view
 PgDown -- page down in tree view

 Backspace -- switch to window with file tree
 q -- exit program

Main window colors:

 WHITE -- path exists only on remote
 CYAN -- path exists only locally 
 YELLOW -- some files from the selected path exist on remote, some - locally
 GREEN -- path fully synchronized between local and remote
'''
