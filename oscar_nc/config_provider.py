###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import json
import os

CONFIG_EXAMPLE = '''
{
    "login": "login",
    "password": "password",
    "web_address": "https://your_nextcloud_address.com",
    "remote_folder": "PATH_TO_REMOTE_FOLDER",
    "local_folder": "PATH_TO_LOCAL_FOLDER",
    "use_cache": true/false,
    "metadata_cache_file": "CACHE_FILE_TO_SAVE_METADATA.json"
}
'''


class ConfigProvider:
    def __init__(self, cfg_path=None, cache_folder=None) -> None:
        if cache_folder is None:
            self.cache_folder = os.path.expanduser("~") + "/.oscar_nc"
        else:
            self.cache_folder = cache_folder

        self.config_not_found_str = f'Config path is not set. Use command "oscar_nc set_config PATH_TO_CONFIG.json", where config file has the following structure:\n {CONFIG_EXAMPLE}'

        if cfg_path is not None:
            self.set_config(cfg_path)

    def get_config_path(self):
        assert os.path.exists(
            f"{self.cache_folder}/config_path.txt"), self.config_not_found_str

        with open(f"{self.cache_folder}/config_path.txt", 'r') as f:
            return f.read()

    def get_config(self):

        assert os.path.exists(
            f"{self.cache_folder}/config_path.txt"), self.config_not_found_str

        config_path = self.get_config_path()

        with open(config_path, 'r') as f:
            cfg = json.load(f)

        return cfg

    def set_config(self, cfg_path):
        os.makedirs(self.cache_folder, exist_ok=True)

        with open(f"{self.cache_folder}/config_path.txt", 'w') as f:
            f.write(os.path.abspath(cfg_path))
