###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import curses

from oscar_nc.gui.help_page import HelpPage
from oscar_nc.gui.main_page import MainPage
from oscar_nc.gui.status_page import LoggingPage

from oscar_nc.utils import no_assert_trace


class GUI:

    def __init__(self, scr) -> None:

        self.stdscr = scr

        self.stdscr.keypad(True)
        curses.nl()
        curses.curs_set(False)

        bottom_win_height = min(20, curses.LINES // 2)
        self.main_window = curses.newwin(
            curses.LINES - bottom_win_height, curses.COLS - 1, 0, 0)

        self.main_window.timeout(200)
        self.main_window.keypad(True)
        self.main_window.border()

        self.bottom_window = curses.newwin(
            bottom_win_height, curses.COLS, curses.LINES - bottom_win_height, 0)

        self.bottom_window.box()

        self.stopped = False

        self.main_page = MainPage(self.main_window)

        self.help_page = HelpPage(
            self.main_window)

        self.status_window = LoggingPage(self.bottom_window)

        self.active_window = self.main_page

        curses.doupdate()

    def __del__(self):
        curses.curs_set(True)
        curses.endwin()
        self.stdscr.keypad(False)

    def handle_input(self):
        ch = self.main_window.getch()
        if ch == -1:
            return
        elif ch in [ord('q')]:
            self.stopped = True
        elif ch in [curses.KEY_BACKSPACE, 127]:
            self.active_window = self.main_page
        elif ch == ord("h"):
            if self.active_window == self.help_page:
                self.active_window = self.main_page
            else:
                self.active_window = self.help_page
        else:
            self.active_window.handle_input(ch)

    def spin(self):
        while not self.stopped:
            self.active_window.update()
            curses.doupdate()
            self.handle_input()


@no_assert_trace
def main():
    curses.wrapper(lambda scr: GUI(scr).spin())


if __name__ == "__main__":
    main()
