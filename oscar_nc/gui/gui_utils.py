###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import enum
from typing import List

from oscar_nc.utils import SynchronizedInfo, prettify_content_size

from oscar_nc.common_data import TAB



class TreeNode:
    def __init__(self, parent, name, data=""):

        self.parent = parent
        self.name = name
        self.data = data
        self.children = {}
        self.expanded = False

    def expand(self):
        self.expanded = not self.expanded

    def iterate(self):
        yield self, 0
        if not self.expanded:
            return
        for child_key in sorted(self.children.keys()):
            for child, depth in self.children[child_key].iterate():
                yield child, depth + 1

    def add_child(self, name: str, data) -> None:
        if not name in self.children:
            self.children[name] = TreeNode(self, name, data)
        else:
            self.children[name].data = data

    def get_child(self, name: str) -> str:
        return self.children[name]

    def get_children(self) -> List[str]:
        return self.children.keys()

    def print_str(self, level) -> None:
        if self.name != "":
            return f"\n{'  ' * level}".join([self.name] + [child.print_str(level + 1) for child in self.children.values()])
        else:
            return f"\n{'  ' * level}".join([child.print_str(level + 1) for child in self.children.values()])

    def render(self):

        if self.data is None:
            return [self.name, "", ""]

        text_fields = [
            (TAB * self.data.depth + self.data.name),
            prettify_content_size(self.data.local_size),
            prettify_content_size(self.data.remote_size),
            # prettify_content_size(
            #     max(self.data.remote_size, self.data.local_size)),
            self.data.last_modified,
            self.data.content_type
        ]
        return text_fields

    def fully_synchronized(self):
        return self.fully_on_local() and self.fully_on_remote() and self.data.local_size == self.data.remote_size

    def partially_synchronized(self):
        return self.partially_on_local() and self.partially_on_remote()

    def fully_on_local(self):
        return self.data.stored_on_local and all(map(lambda child: child.fully_on_local(), self.children.values()))

    def fully_on_remote(self):
        return self.data.stored_on_remote and all(map(lambda child: child.fully_on_remote(), self.children.values()))

    def partially_on_local(self):
        return self.data.stored_on_local or any(map(lambda child: child.fully_on_local(), self.children.values()))

    def partially_on_remote(self):
        return self.data.stored_on_remote or any(map(lambda child: child.fully_on_remote(), self.children.values()))

    def get_path(self):
        return self.data.full_path

    def set_downloaded(self):
        self.data.stored_on_local = True
        for child in self.children.values():
            child.set_downloaded()

    def set_uploaded(self):
        self.data.stored_on_remote = True
        for child in self.children.values():
            child.set_uploaded()


class FileTree:
    def __init__(self, files_data) -> None:
        self.root = TreeNode(None, "./", None)
        self.skip_root = True

        for f in sorted(files_data, key=lambda el: el.full_path):
            self.add_child(f.full_path.split('/'), f)

        self.root.expand()

    def add_child(self, node_names: List[str], data):
        cur_node = self.root
        for name in node_names:
            if name in cur_node.children:
                cur_node = cur_node.children[name]
            else:
                cur_node.add_child(name, data)
                return
        cur_node.data = data

    def get_children(self, node_names: List[str]) -> List[str]:
        current_node = self.root
        for name in node_names:
            current_node = current_node.get_child(name)
        return list(current_node.get_children())

    def iterate(self):

        for idx, iterator in enumerate(self.root.iterate()):
            if self.skip_root and idx == 0:
                continue
            yield iterator

    def update_data(self, files_info: List[SynchronizedInfo]):

        for f in sorted(files_info, key=lambda el: el.full_path):
            # parents = os.path.dirname(
            #     f.full_path) if f.content_type != "folder" else f.full_path
            # parents = parents.split("/")
            self.add_child(f.full_path.split('/'), f)

    def __repr__(self) -> str:
        return self.root.print_str(0)


class text_color(enum.IntEnum):
    COMMON = 1
    FULL_SYNC = 2
    PART_SYNC = 3
    ONLY_LOCAL = 4
    ONLY_REMOTE = 5
    HELP = 6
