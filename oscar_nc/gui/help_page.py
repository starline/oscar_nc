###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import curses
from oscar_nc.config_provider import ConfigProvider

from oscar_nc.gui.gui_utils import text_color
from copy import deepcopy as dc

from oscar_nc.utils import TAB
from oscar_nc.common_data import GUI_HELP_STR


class HelpPage:

    def __init__(self, window) -> None:

        self.window = window

        self.cfg = ConfigProvider().get_config()

        self.help_info = GUI_HELP_STR.split("\n")

        bg_color = curses.COLOR_BLACK
        common_color = curses.COLOR_WHITE

        self.MAX_LINES, self.MAX_COLS = None, None

        self.update()

        curses.init_pair(text_color.HELP, common_color, bg_color)

    def draw_line(self, line, col, text, *args, **kwargs):

        text = text[:min(self.MAX_COLS, len(text))]
        if (0 <= line <= self.MAX_LINES - 2) and (0 <= col <= self.MAX_COLS - 2):
            self.window.addstr(line, col,
                               text, *args, **kwargs)

    def draw_header(self):

        self.window.attrset(curses.color_pair(text_color.HELP))
        title = "OSCAR NextCloud"
        self.draw_line(0, (self.MAX_COLS - len(title)) // 2, title)

    def update(self):

        self.MAX_LINES, self.MAX_COLS = self.window.getmaxyx()

        self.window.clear()

        if self.MAX_LINES <= 0 or self.MAX_COLS <= 0:
            return

        self.draw_header()

        lines_to_draw = dc(self.help_info)
        lines_to_draw += ["", "-" *
                          (self.MAX_COLS - 1), "", "Current config:", ""]

        for key, val in self.cfg.items():
            lines_to_draw.append(f'{TAB}{key}: {val}')

        for idx, line in enumerate(lines_to_draw[:self.MAX_LINES]):
            self.draw_line(idx, 0, line)

        self.window.noutrefresh()

    def handle_input(self, ch):
        pass
