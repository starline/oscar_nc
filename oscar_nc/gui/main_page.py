###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import curses
import shutil
from multiprocessing.pool import ThreadPool

from oscar_nc.gui.gui_utils import FileTree, TreeNode, text_color
from oscar_nc.nc_synchronizer import DataSynchronizer
from oscar_nc.utils import get_total_size, prettify_content_size


class MainPage:

    def __init__(self, window, auto_update=True) -> None:

        self.auto_update = auto_update
        self.window = window

        self.data_synchronizer = DataSynchronizer()

        self.files_info_tree = FileTree(
            self.data_synchronizer.get_synchronized_info())

        bg_color = curses.COLOR_BLACK

        common_color = curses.COLOR_BLUE
        remote_color = curses.COLOR_WHITE
        local_color = curses.COLOR_CYAN
        full_color = curses.COLOR_GREEN
        partial_color = curses.COLOR_YELLOW
        curses.init_pair(text_color.COMMON, common_color, bg_color)
        curses.init_pair(text_color.ONLY_REMOTE, remote_color, bg_color)
        curses.init_pair(text_color.ONLY_LOCAL, local_color, bg_color)
        curses.init_pair(text_color.FULL_SYNC, full_color, bg_color)
        curses.init_pair(text_color.PART_SYNC, partial_color, bg_color)

        self.header_offset = 2
        self.footer_offset = 1
        self.curidx = 2
        self.max_lines = 1
        self.counter = 0

        self.column_names = ['name', "local size",
                             "remote size", "Last modified", "type"]
        self.offsets = [60, 15, 15, 30, 5]

        self.selected_node = None
        self.last_command_status = ""

        self.MAX_LINES, self.MAX_COLS = self.window.getmaxyx()

        self.task_thread = None

        self.thread_pool = ThreadPool(processes=1)

        self.update()

    def __del__(self):

        if (hasattr(self, "thread_pool")):
            self.thread_pool.close()

    def draw_line(self, line, col, text, *args, **kwargs):

        text = text[:min(self.MAX_COLS - col - 1, len(text))]

        text = text + (" " * (self.MAX_COLS - col - len(text) - 1))
        self.window.addstr(line, col,
                           text, *args, **kwargs)

    def draw_array(self, line, fields):
        line_str = "".join(
            [f'{s:{offset}}' for s, offset in zip(fields, self.offsets)])
        self.draw_line(line, 0, line_str)

    def set_line_color(self, line, node: TreeNode):

        attrs = 0

        if line == self.curidx:
            attrs |= curses.A_BOLD | curses.A_UNDERLINE

        if node.fully_synchronized():
            attrs |= curses.color_pair(text_color.FULL_SYNC)

        elif node.partially_synchronized():
            attrs |= curses.color_pair(text_color.PART_SYNC)

        elif node.fully_on_local() and not node.fully_on_remote():
            attrs |= curses.color_pair(text_color.ONLY_LOCAL)

        elif node.fully_on_remote():
            attrs |= curses.color_pair(text_color.ONLY_REMOTE)

        self.window.attrset(attrs)

    def draw_header(self):

        self.window.attrset(curses.color_pair(text_color.COMMON))
        title = "OSCAR NextCloud"

        self.draw_line(0, max(0, (self.MAX_COLS - len(title)) // 2), title)
        self.draw_array(1, self.column_names)

        hotkeys = "Keys: s - sync, u - update, h - help"

        free_space = prettify_content_size(
            shutil.disk_usage(self.data_synchronizer.local_folder)[2])

        remote_disk_usage = get_total_size(
            self.data_synchronizer.update_remote_info())
        remote_disk_usage = prettify_content_size(remote_disk_usage)

        local_disk_usage = get_total_size(
            self.data_synchronizer.update_local_info())
        local_disk_usage = prettify_content_size(local_disk_usage)

        disk_space = f'Local: {local_disk_usage}, Remote: {remote_disk_usage}, Local free: {free_space}'

        footer = disk_space + " " * \
            max(0, (self.MAX_COLS - len(disk_space) - len(hotkeys) - 1)) + hotkeys

        self.draw_line(self.MAX_LINES - 1, 0, footer)

    def update(self):

        self.window.clear()

        self.MAX_LINES, self.MAX_COLS = self.window.getmaxyx()
        if self.MAX_LINES <= 0 or self.MAX_COLS <= 0:
            return

        self.draw_header()

        self.curidx %= self.max_lines
        if self.curidx < self.header_offset:
            self.curidx = self.header_offset
        line = self.header_offset
        offset = max(0, self.curidx - self.MAX_LINES + 5)

        for node, _ in self.files_info_tree.iterate():

            if line == self.curidx:
                self.selected_node = node

            self.set_line_color(line, node)

            if 0 <= line - offset <= self.MAX_LINES - 1 - self.footer_offset:
                self.draw_array(line - offset, node.render())

            line += 1

        self.max_lines = line

        self.window.noutrefresh()

    def handle_input(self, ch):

        if ch == curses.KEY_UP:
            self.curidx -= 1

        elif ch == curses.KEY_DOWN:
            if self.curidx < self.max_lines - 1:
                self.curidx += 1

        elif ch == curses.KEY_PPAGE:
            self.curidx -= self.MAX_LINES
            if self.curidx < self.header_offset:
                self.curidx = self.header_offset

        elif ch == curses.KEY_NPAGE:
            self.curidx += self.MAX_LINES
            if self.curidx >= self.max_lines:
                self.curidx = self.max_lines - 1

        elif ch == curses.KEY_ENTER or ch == 10 or ch == 13:
            self.selected_node.expand()

        elif ch == ord('u'):
            self.update_info()

        elif ch == ord('s'):
            self.synchronize_files()

    def update_info(self, quite=False):

        if not quite:
            print("Updating info from remote...")

        self.files_info_tree.update_data(
            self.data_synchronizer.get_synchronized_info(use_remote_cache=False))

        if not quite:
            print("Done")

    def synchronize_files(self):

        if self.selected_node is None:
            print(f"No selected folder for")

        path = self.selected_node.get_path()

        if not self.selected_node.fully_synchronized():

            def sync_with_update(path):
                self.data_synchronizer.synchronize(path)
                self.update_info(quite=True)

            if self.auto_update:
                task = sync_with_update
            else:
                task = self.data_synchronizer.synchronize

            self.thread_pool.apply_async(task, args=(path,))
        else:
            print(f'Path {path} is synchronized')
