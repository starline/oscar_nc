###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

from collections import deque
import curses
import re
import sys

from oscar_nc.gui.gui_utils import text_color


class LoggingPage:

    def __init__(self, window) -> None:

        self.window = window

        bg_color = curses.COLOR_BLACK
        common_color = curses.COLOR_WHITE

        self.MAX_LINES, self.MAX_COLS = self.window.getmaxyx()

        self.stored_logs = deque(maxlen=self.MAX_LINES)

        curses.init_pair(text_color.HELP, common_color, bg_color)

        sys.stdout = self
        sys.stderr = self

        self.update()

    def write(self, buf):
        ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
        buf = ansi_escape.sub('', buf)
        if buf in ['', "\n"]:  # skip tqdm escape characters and empty strings
            return

        for line in buf.strip().splitlines():
            self.stored_logs.append(line)
        self.update()

    def flush(self):
        pass

    def draw_line(self, line, col, text, *args, **kwargs):

        text = text[:min(self.MAX_COLS, len(text))]

        self.window.addstr(line, col,
                           text, *args, **kwargs)

    def background_render(self):
        self.window.border()
        self.draw_line(0, 1, " Log ")

    def update(self):

        self.MAX_LINES, self.MAX_COLS = self.window.getmaxyx()
        self.window.clear()

        if self.MAX_LINES <= 0 or self.MAX_COLS <= 0:
            return

        self.background_render()

        for idx, line in enumerate(list(self.stored_logs)[-self.MAX_LINES + 2:]):
            self.draw_line(idx + 1, 0 + 1, line)

        self.window.noutrefresh()
