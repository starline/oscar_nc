###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import asyncio
from itertools import starmap
import os
import sys
import requests
from http import HTTPStatus


from tqdm import tqdm
import tqdm.asyncio

from aiohttp import ClientSession, BasicAuth, ClientPayloadError
import aiofiles

from oscar_nc.common_data import SHOW_SIZE_PROGRESS_THRESHOLD_MB, PROPFIND_REQUEST, DOWNLOAD_CHUNK_SIZE, READ_CHUNK_SIZE


async def download_file(session, url, local_path):

    os.makedirs(os.path.dirname(local_path), exist_ok=True)

    response = await session.request("GET", url, chunked=True)

    if "Content-Length" in response.headers:
        total_size = int(response.headers["Content-Length"]) / 1024**2
        disable_pbar = total_size < SHOW_SIZE_PROGRESS_THRESHOLD_MB
    else:
        total_size = None
        disable_pbar = True

    async with aiofiles.open(local_path, mode='wb') as f:

        pbar = tqdm.asyncio.tqdm(
            total=total_size,
            leave=False, unit="MB", disable=disable_pbar,
            dynamic_ncols=True,
            bar_format="{l_bar}{bar}{n:.2f}/{total:.2f} {unit} [{elapsed}<{remaining}, {rate_fmt}{postfix}]")
        try:
            async for data in response.content.iter_chunked(DOWNLOAD_CHUNK_SIZE):
                pbar.update(len(data) / 1024**2)
                await f.write(data)
        except ClientPayloadError as e:
            print(e)
            print("Saving file error:", local_path)

        pbar.close()

    return HTTPStatus(response.status)


async def file_sender(filename):

    pbar = tqdm.asyncio.tqdm(total=os.path.getsize(filename),
                             unit="MB",
                             unit_scale=1 / 1024**2,
                             bar_format="{l_bar}{bar}{n:.2f}/{total:.2f} {unit} [{elapsed}<{remaining}, {rate_fmt}{postfix}]",
                             disable=os.path.getsize(
                                 filename) / 1024**2 < SHOW_SIZE_PROGRESS_THRESHOLD_MB
                             )
    async with aiofiles.open(filename, mode='rb') as f:
        chunk = await f.read(READ_CHUNK_SIZE)
        while chunk:
            pbar.update(len(chunk))
            yield chunk
            chunk = await f.read(READ_CHUNK_SIZE)
        pbar.close()


async def upload_file(session, url, local_path):
    response = await session.put(url,
                                 data=file_sender(local_path),
                                 )
    return HTTPStatus(response.status)


async def create_folder(session, folder):
    response = await session.request("MKCOL", folder)
    return HTTPStatus(response.status)


async def get_files_info(session, url):
    response = await session.request(
        method="PROPFIND",
        url=url,
        data=PROPFIND_REQUEST,
        headers={"Depth": "infinity"},
    )
    return (HTTPStatus(response.status), await response.text())


async def handle_requests(auth, function, *args, no_parallel=False, quite=False):
    results = []

    async with ClientSession(auth=auth) as session:

        tasks = list(
            starmap(lambda *inputs: function(session, *inputs), zip(*args)))

        tqdm_class = tqdm.tqdm if no_parallel else tqdm.asyncio.tqdm.as_completed

        pbar = tqdm_class(tasks, position=0,
                          desc=function.__name__, disable=quite)

        for future in pbar:
            results.append(await future)

    return results


def get_or_create_event_loop():
    # taken from https://techoverflow.net/2020/10/01/how-to-fix-python-asyncio-runtimeerror-there-is-no-current-event-loop-in-thread/
    try:
        return asyncio.get_event_loop()
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            return asyncio.get_event_loop()


def execute_requests(*args, show_statuses=False, **kwargs):
    if sys.version_info >= (3, 7):
        results = asyncio.run(
            handle_requests(*args, **kwargs))
    else:
        loop = get_or_create_event_loop()
        results = loop.run_until_complete(handle_requests(*args, **kwargs))

    if show_statuses:
        [print(res.name, res.value)
         for res in results if isinstance(res, HTTPStatus)]

    return results


def url_encode(paths):
    return [requests.utils.requote_uri(path) for path in paths]


class NextCloudClient:
    def __init__(self, login, password) -> None:
        self.login = login
        self.password = password

        self.auth = BasicAuth(self.login, self.password)

    def download_files(self, remote_paths, local_paths, show_statuses=False):
        return execute_requests(self.auth, download_file,
                                url_encode(remote_paths),
                                local_paths,
                                show_statuses=show_statuses)

    def upload_files(self, remote_paths, local_paths, show_statuses=False):
        return execute_requests(self.auth,
                                upload_file,
                                url_encode(remote_paths),
                                local_paths,
                                show_statuses=show_statuses)

    def create_folders(self, folders, show_statuses=False):
        return execute_requests(self.auth,
                                create_folder,
                                folders,
                                show_statuses=show_statuses,
                                no_parallel=False)

    def get_file_tree(self, url):
        return execute_requests(self.auth,
                                get_files_info,
                                url_encode([url]), quite=True)[0]
