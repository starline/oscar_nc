###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


import argparse
from http import HTTPStatus
import os
from pprint import pprint
import shutil
import sys

from oscar_nc.nc_client import NextCloudClient
from oscar_nc.common_data import CLI_HELP_STR, FILE_PATH_DEFAULT_PREFIX
from oscar_nc.config_provider import ConfigProvider

from oscar_nc.utils import count_positional_args, \
    create_dirs, \
    create_synchronized_files_info, \
    get_folders_hierarchy, \
    get_local_files, \
    have_same_parent, \
    merge_paths, \
    prettify_content_size, \
    print_syncronized_tree, \
    load_local_files_dict, \
    prepare_files_list, \
    parse_meta_data_response, \
    Cache, \
    no_assert_trace


class DataSynchronizer:
    def __init__(self, cfg_path: str = None) -> None:

        if isinstance(cfg_path, str) or cfg_path is None:
            cfg_provider = ConfigProvider(cfg_path)
            cfg = cfg_provider.get_config()
        elif isinstance(cfg_path, dict):
            cfg = cfg_path

        self.login = cfg['login']
        self.password = cfg['password']
        self.address = f'{cfg["web_address"]}/{FILE_PATH_DEFAULT_PREFIX}/{self.login}'
        self.remote_folder = cfg['remote_folder']
        self.metadata_cache_file = cfg['metadata_cache_file']
        self.local_folder = cfg['local_folder']

        assert os.path.isabs(
            self.local_folder), f'Local data folder has relative path: {self.local_folder}. Use absolute path instead'

        assert os.path.exists(
            self.local_folder), f'Local data folder does not exist: {self.local_folder}'

        assert self.remote_folder not in [
            "./", "."], f"Please, use another folder, rather than root, got: {self.remote_folder}"

        self.files_info = {}
        self.properties_xml = None

        self.strip_prefix = f"{FILE_PATH_DEFAULT_PREFIX}/{self.login}/{self.remote_folder}/"

        self.nc_client = NextCloudClient(self.login, self.password)
        self.response_cache = Cache(self.metadata_cache_file)

    def update_remote_info(self, use_cache=True):

        if use_cache and self.response_cache.has_value(self.remote_folder):
            return self.response_cache.load(self.remote_folder)

        status, text = self.nc_client.get_file_tree(
            f"{self.address}/{self.remote_folder}")

        assert status in [
            HTTPStatus.OK, HTTPStatus.MULTI_STATUS], f"Response while updating remote info: {status.name}"

        remote_info = parse_meta_data_response(text, self.strip_prefix)
        self.response_cache.save(self.remote_folder, remote_info)

        return remote_info

    def update_local_info(self):
        return load_local_files_dict(self.local_folder)

    def print_tree(self, hide_files=True):
        print_syncronized_tree(
            self.update_remote_info(),
            self.update_local_info(),
            hide_files=hide_files)

    def get_synchronized_info(self, use_remote_cache=True):
        return create_synchronized_files_info(
            self.update_remote_info(use_cache=use_remote_cache),
            self.update_local_info(),
            hide_files=False)

    def download(self, path):
        '''
        path: remote relative path

        downloads file/folder from remote path to local path.
        '''

        # TODO: check disk space before downloading

        remote_files_info = self.update_remote_info()
        local_files_info = self.update_local_info()

        files = prepare_files_list(path, remote_files_info)

        if len(files) == 0:
            print(f"No files found on remote with path: {path}")
            return []

        remote_files = merge_paths(
            self.address + '/' + self.remote_folder, files)

        local_files = merge_paths(self.local_folder, files)

        keep_inds = [idx for idx, f in enumerate(files) if not (f in local_files_info) or (
            local_files_info[f]['size'] != remote_files_info[f]['size'])]

        local_files = [local_files[i] for i in keep_inds]
        remote_files = [remote_files[i] for i in keep_inds]

        download_size = int(remote_files_info[path]['size'])
        free_space = shutil.disk_usage(self.local_folder)[2]

        if download_size > free_space:
            print(
                f'Not enough disk space for downloading. Remains: {prettify_content_size(free_space)}, need: {prettify_content_size(download_size)}')
            return []

        if len(local_files) == 0:
            print("All files are already downloaded")
            return []

        create_dirs(local_files)

        result_statuses = self.nc_client.download_files(
            remote_files, local_files)

        if all(map(lambda status: status in [HTTPStatus.OK], result_statuses)):
            print(f" {len(result_statuses)} files are downloaded")
        else:
            print("Errors while downloading:")
            for st, local_f in zip(result_statuses, local_files):
                if st not in [HTTPStatus.OK]:
                    print(f'  {local_f} - {st}')

        return result_statuses

    def upload(self, local_path):
        # 1. check if file exists in saved cache
        # 2. create folders on remote if they do not exist
        # 4. send all files

        # TODO: can we check left free space on Nextcloud???

        upload_files = get_local_files(
            f'{self.local_folder}/{local_path}', strip_prefix=f"{self.local_folder}/")

        if len(upload_files) == 0:
            print(
                f"No files found locally for uploading with path: {local_path}")
            return []

        remote_files_info = self.update_remote_info()
        upload_files = list(
            filter(lambda f: not f.replace(f"{self.local_folder}/", "") in remote_files_info, upload_files))

        if len(upload_files) == 0:
            print("All files are already uploaded")
            return []

        remote_files = merge_paths(
            self.address + '/' + self.remote_folder, upload_files)
        local_files = merge_paths(self.local_folder, upload_files)

        create_dirs_statuses = self.create_folders(local_files)
        if any(map(lambda st: not st in [HTTPStatus.CREATED, HTTPStatus.METHOD_NOT_ALLOWED], create_dirs_statuses)):
            print(
                f'Create directories on remote failed: {create_dirs_statuses}')
            return []

        result_statuses = self.nc_client.upload_files(
            remote_files, local_files)

        if all(map(lambda status: status in [HTTPStatus.CREATED, HTTPStatus.NO_CONTENT], result_statuses)):
            print(f" {len(result_statuses)} files are uploaded")
        else:
            print("Errors while uploading:")
            for st, local_f in zip(result_statuses, local_files):
                if st not in [HTTPStatus.CREATED, HTTPStatus.NO_CONTENT]:
                    print(f'  {local_f} - {st}')

        return result_statuses

    def synchronize(self, path):
        self.upload(path)
        self.download(path)

    def create_folders(self, local_files):
        remote_files_info = self.update_remote_info()

        folders_hierarchy = get_folders_hierarchy(
            local_files, strip_prefix=f"{self.local_folder}/")

        res_statuses = []
        for current_folders in folders_hierarchy:

            not_uploaded_folders = [
                f for f in current_folders if not f in remote_files_info]

            not_uploaded_folders = merge_paths(
                self.address + '/' + self.remote_folder, not_uploaded_folders)

            if len(not_uploaded_folders) == 0:
                continue

            statuses = self.nc_client.create_folders(
                not_uploaded_folders, show_statuses=False)

            res_statuses += statuses
        return res_statuses

    def complete_paths(self, current_path):
        remote_files_info = self.update_remote_info()
        local_files_info = load_local_files_dict(self.local_folder)

        complete_candidates = set()

        [complete_candidates.add(key) for key in remote_files_info.keys() if (
            key.startswith(current_path)) and have_same_parent(current_path, key)]

        [complete_candidates.add(key)
         for key in local_files_info.keys() if (key.startswith(current_path)) and have_same_parent(current_path, key)]

        merged_paths = " ".join(
            map(lambda el: f'"{el}"', sorted(complete_candidates)))
        print(merged_paths)


@no_assert_trace
def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("action", choices=[
                        "list", "update", "download", "upload", "set_config", "get_config", "complete", "help"], help="Action to perform")
    parser.add_argument(
        "path", help="path for download/upload/set_config/complete actions. Leave empty for another actions")

    parser.add_argument('--show_files', action="store_true")

    # a hack to "simulate" optional positional second CLI argument
    if count_positional_args() == 2 and sys.argv[1] in ["list", "update", "get_config", "complete", "help"]:
        sys.argv.append("")

    args = parser.parse_args()

    if args.action == "help":
        print(CLI_HELP_STR)
        return

    if args.action == "set_config":
        cfg_provider = ConfigProvider()
        cfg_provider.set_config(args.path)
        return
    elif args.action == "get_config":
        cfg_provider = ConfigProvider()
        print("Path:", cfg_provider.get_config_path())
        pprint(cfg_provider.get_config())
        return

    dataManager = DataSynchronizer()

    if args.action == "list":
        dataManager.print_tree(hide_files=not args.show_files)

    elif args.action == "update":
        dataManager.update_remote_info(use_cache=False)

    elif args.action == "download":
        dataManager.download(args.path)

    elif args.action == "upload":
        dataManager.upload(args.path)

    elif args.action == "complete":
        dataManager.complete_paths(args.path)


if __name__ == "__main__":

    main()
