###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


from datetime import datetime, timedelta, timezone
import json
import os
from pathlib import Path
import sys
from xml.dom import minidom
import pytz
import requests

from dataclasses import dataclass

from oscar_nc.common_data import ONLY_LOCAL_COLOR, ONLY_REMOTE_COLOR, EVERYWHERE_COLOR, TAB


# Painting was taken from https://github.com/termcolor/termcolor
COLORS = {
    "black": 30,
    "grey": 30,  # Actually black but kept for backwards compatibility
    "red": 31,
    "green": 32,
    "yellow": 33,
    "blue": 34,
    "magenta": 35,
    "cyan": 36,
    "light_grey": 37,
    "dark_grey": 90,
    "light_red": 91,
    "light_green": 92,
    "light_yellow": 93,
    "light_blue": 94,
    "light_magenta": 95,
    "light_cyan": 96,
    "white": 97,
}

# Painting was taken from https://github.com/termcolor/termcolor


def colored(
    text: str,
    color: str
) -> str:
    """Colorize text.

    Available text colors:
        black, red, green, yellow, blue, magenta, cyan, white,
        light_grey, dark_grey, light_red, light_green, light_yellow, light_blue,
        light_magenta, light_cyan.

    Available text highlights:
        on_black, on_red, on_green, on_yellow, on_blue, on_magenta, on_cyan, on_white,
        on_light_grey, on_dark_grey, on_light_red, on_light_green, on_light_yellow,
        on_light_blue, on_light_magenta, on_light_cyan.

    Available attributes:
        bold, dark, underline, blink, reverse, concealed.

    Example:
        colored('Hello, World!', 'red', 'on_black', ['bold', 'blink'])
        colored('Hello, World!', 'green')
    """

    fmt_str = "\033[%dm%s"
    if color is not None:
        text = fmt_str % (COLORS[color], text)

    return text + "\033[0m"


@dataclass
class SynchronizedInfo:
    name: str
    full_path: str
    local_size: int = 0
    remote_size: int = 0
    content_type: str = ""
    last_modified: str = ""
    stored_on_remote: bool = False
    stored_on_local: bool = False
    depth: int = 0


def get_color(stored_on_remote, stored_on_local):
    if stored_on_remote and not stored_on_local:
        color = ONLY_REMOTE_COLOR
    elif not stored_on_remote and stored_on_local:
        color = ONLY_LOCAL_COLOR
    elif stored_on_remote and stored_on_local:
        color = EVERYWHERE_COLOR
    else:
        raise RuntimeError(f"file not found in remote and locally")

    return color


def get_folder_size(folder):
    sz = sum([os.path.getsize(f'{root}/{f}') for (root, _, files) in os.walk(folder)
              for f in files])
    return sz


def get_modification_time(path):
    modif_ts = os.path.getmtime(path)
    modif_time = datetime.fromtimestamp(
        modif_ts, tz=timezone(timedelta(hours=3)))
    return modif_time.strftime(f"%a, %d %b %Y %H:%M:%S")


def load_local_files_dict(local_path):
    local_files = {}

    for (root, folders, files) in os.walk(local_path):
        for folder in folders:

            f_key = f'{root}/{folder}'.replace(local_path + "/", "")

            local_files[f_key] = {
                "content_type": "folder",
                "last_modified": get_modification_time(f'{root}/{folder}'),
                "size": get_folder_size(f'{root}/{folder}')
            }

        for f in files:
            f_key = f'{root}/{f}'.replace(local_path + "/", "")
            local_files[f_key] = {
                "content_type": "file",
                "last_modified": get_modification_time(f'{root}/{f}'),
                "size": os.path.getsize(f'{root}/{f}')
            }
    return local_files


def create_synchronized_files_info(remote_files_dict, local_files_dict, hide_files=True):
    merged_files_dict = {**remote_files_dict, **local_files_dict}

    remote_files_list = set(remote_files_dict.keys())
    local_files_list = set(local_files_dict.keys())

    files_info = []

    for f_key in sorted(merged_files_dict.keys()):

        f = merged_files_dict[f_key]

        is_folder = f['content_type'] == 'folder'

        if hide_files and not is_folder:
            continue

        if f_key.endswith("/"):
            f_key = f_key[:-1]
        basename = f_key.split('/')[-1]

        path_len = len(f_key.split('/')) - 1

        files_info.append(SynchronizedInfo(
            name=basename,
            full_path=f_key,
            depth=path_len,
            content_type=f['content_type'],
            stored_on_local=f_key in local_files_list,
            stored_on_remote=f_key in remote_files_list,
            local_size=0 if not f_key in local_files_list else int(
                local_files_dict[f_key]['size']),
            remote_size=0 if not f_key in remote_files_list else int(
                remote_files_dict[f_key]['size']),
            last_modified=f["last_modified"]
        ))

    return files_info


def print_syncronized_tree(remote_files_dict, local_files_dict, hide_files=True):

    for file_info in create_synchronized_files_info(remote_files_dict, local_files_dict, hide_files=hide_files):

        sz = max(file_info.local_size, file_info.remote_size) // 1024**2

        text = f"{(TAB * file_info.depth + file_info.name):40s} - {sz:5d} MB - {file_info.last_modified}"
        color = get_color(file_info.stored_on_remote,
                          file_info.stored_on_local)
        print(colored(text, color))


def get_size(resp):
    sz = resp.getElementsByTagName('oc:size')[0].firstChild.data
    return sz


def get_content_type(resp):
    content = resp.getElementsByTagName('d:getcontenttype')
    if content[0] is not None and content[0].firstChild is not None:
        return "file"
    else:
        return "folder"


def get_last_modified_time(resp):

    modif_time = datetime.strptime(
        resp.getElementsByTagName('d:getlastmodified')[0].firstChild.data, f"%a, %d %b %Y %H:%M:%S %Z")

    modif_time = modif_time.replace(tzinfo=pytz.UTC)
    modif_time = modif_time.astimezone(datetime.now().astimezone().tzinfo)
    return modif_time.strftime(f"%a, %d %b %Y %H:%M:%S")


class Cache:
    def __init__(self, cache_file) -> None:
        self.cache_file = cache_file

        if not os.path.exists(cache_file):
            self.data = {}
        else:
            with open(self.cache_file, 'r') as f:
                self.data = json.load(f)

    def save(self, name, data):

        self.data[name] = data

        dir = os.path.dirname(self.cache_file)
        if dir != "":
            os.makedirs(dir, exist_ok=True)

        with open(self.cache_file, 'w') as f:
            json.dump(self.data, f)

    def has_value(self, name):
        return name in self.data

    def load(self, name):
        assert name in self.data

        return self.data[name]


def get_local_files(path, strip_prefix=""):

    if not os.path.exists(path):
        return []

    if os.path.isfile(path):
        return [path.replace(strip_prefix, "")]

    files_list = []

    for (root, dirs, files) in os.walk(path):
        [files_list.append(f'{root}/{f}'.replace(strip_prefix, ""))
         for f in files]

    return sorted(files_list)


def prepare_files_list(path, files_info):
    if path not in files_info:
        return []

    if files_info[path]['content_type'] == 'folder':

        files = [fname for fname, props in files_info.items()
                 if fname.startswith(path) and props['content_type'] != "folder"]
    else:
        files = [path]

    files = sorted(files)

    return files


def parse_meta_data_response(response_text, strip_prefix):

    files_info = {}
    properties_xml = minidom.parseString(response_text)

    # print(properties_xml.toprettyxml())

    for resp in properties_xml.getElementsByTagName('d:response'):
        path = requests.utils.unquote(
            resp.getElementsByTagName('d:href')[0].firstChild.data)

        path = path.replace(strip_prefix, "")
        if path.endswith("/"):
            path = path[:-1]

        if path == "":
            continue

        files_info[path] = {
            "content_type": get_content_type(resp),
            "last_modified": get_last_modified_time(resp),
            "size": get_size(resp),
        }

    return files_info


def create_local_file(fname, fsize_mb):

    os.makedirs(os.path.dirname(fname), exist_ok=True)
    with open(fname, 'wb') as f:
        f.truncate(int(fsize_mb * 1024**2))


def get_folders_hierarchy(files, strip_prefix=""):
    '''
    for files extract all directories and combine them in list of lists according to their depth in ascending order
    For example, for files [a/b/c/d/file.txt, a/b/c/e/file2.txt] the function returns
    [
        ["a"],
        ["a/b"],
        ["a/b/c"],
        ["a/b/c/d", "a/b/c/e"]
    ]
    '''

    unique_dirs = set()
    for f in files:
        for parent in reversed(Path(f.replace(strip_prefix, "")).parents):
            unique_dirs.add(str(parent))

    unique_dirs = sorted(list(unique_dirs))
    unique_dirs = list(filter(lambda el: not el.endswith("."), unique_dirs))

    for prefix in strip_prefix.split("/"):
        if unique_dirs.count(prefix) > 0:
            unique_dirs.remove(prefix)

    depths = [d.count("/") for d in unique_dirs]

    dirs_hierarchy = []

    for depth in range(min(depths), max(depths) + 1):
        current_depth_dirs = list(
            filter(lambda path: path.count("/") == depth, unique_dirs))
        if len(current_depth_dirs) > 0:
            dirs_hierarchy.append(current_depth_dirs)

    return dirs_hierarchy


def have_same_parent(path1, path2):
    path1 = path1 + '.' if path1.endswith("/") else path1
    path2 = path2 + '.' if path2.endswith("/") else path2
    return os.path.dirname(path1) == os.path.dirname(path2)


def count_positional_args():
    return sum([not arg.startswith("-") for arg in sys.argv])


def remove_existing_files(save_paths):
    keep_paths = []
    for path in save_paths:
        if not os.path.exists(path):
            keep_paths.append(path)
    return keep_paths


def merge_paths(prefix, folders):
    return [f'{prefix}/{folder}' for folder in folders]


def create_dirs(local_files):
    for f in local_files:
        os.makedirs(os.path.dirname(f), exist_ok=True)


def get_download_size(keep_files, remote_files_info):
    sz = 0
    for f in keep_files:
        sz += remote_files_info[f]['size']
    return sz


def get_total_size(remote_files_info):

    prefixes = []
    sz = 0
    for f in sorted(remote_files_info):
        if any(map(lambda el: f.startswith(el), prefixes)):
            continue

        sz += int(remote_files_info[f]['size'])
        prefixes.append(f)

    return sz


def prettify_content_size(size_bytes):
    current_val = int(size_bytes)
    units = ["B", "KB", "MB", "GB"]

    for unit in units:
        if current_val < 1000:
            return f'{current_val:.2f} {unit}'
        current_val /= 1024
    return f'{int(current_val)} {unit:7s}'


def no_assert_trace(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except AssertionError as e:
            print(e)

    return wrapper
