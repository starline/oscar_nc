###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import pytest
import json
import os

from oscar_nc.nc_synchronizer import DataSynchronizer

from oscar_nc.utils import create_local_file

script_dir = os.path.dirname(os.path.realpath(__file__))


def pytest_addoption(parser):
    parser.addoption("--cfg", type=str, default=None,
                     help="config file to use in tests")


def pytest_collection_modifyitems(config, items):

    if config.getoption("--cfg"):
        return

    for item in items:
        if "requires_auth" in item.keywords:
            item.add_marker(pytest.mark.skip(
                reason="Config file for connecting to remote server not specified. Skipping tests that require authorization"))


@pytest.fixture(scope="session")
def cfg_file(request):

    cfg_arg = request.config.getoption("--cfg")

    return script_dir + "/default_config.json" if cfg_arg is None else cfg_arg


@pytest.fixture(scope="session")
def cfg(cfg_file, local_folder):
    with open(cfg_file, 'r') as f:
        cfg = json.load(f)

    cfg['local_folder'] = local_folder

    return cfg


@pytest.fixture(scope="session")
def local_folder(tmp_path_factory):
    return str(tmp_path_factory.mktemp("local_folder"))


@pytest.fixture(scope="session")
def local_upload_folder(local_folder):

    files_folder = local_folder + '/upload_folder'
    os.makedirs(files_folder)

    return files_folder


@pytest.fixture(scope="session")
def local_download_folder(local_folder):

    files_folder = local_folder + '/download_folder'
    os.makedirs(files_folder)
    return files_folder


@pytest.fixture(scope="session")
def temp_local_files(local_upload_folder):

    [create_local_file(f'{local_upload_folder}/file_{i}.txt', 0.2)
     for i in range(5)]

    return [f"upload_folder/file_{i}.txt" for i in range(5)]


@pytest.fixture
def dataSynchronizer(cfg):
    return DataSynchronizer(cfg)


@pytest.fixture(scope="session")
def default_cfg(local_folder):
    with open(script_dir + "/default_config.json", 'r') as f:
        cfg = json.load(f)

    cfg['local_folder'] = local_folder

    return cfg


@pytest.fixture
def defaultDataSynchronizer(default_cfg):
    return DataSynchronizer(default_cfg)
