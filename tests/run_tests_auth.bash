#! /usr/bin/env bash

pytest -n auto -v --show-capture all --cfg test_config.json .
