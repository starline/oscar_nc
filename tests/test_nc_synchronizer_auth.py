###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
from http import HTTPStatus
import pytest


@pytest.mark.requires_auth
class TestWithAuthClass:

    def test_remote_tree(self, dataSynchronizer):

        remote_data = dataSynchronizer.update_remote_info(use_cache=False)

        assert remote_data is not None

    def test_upload(self, dataSynchronizer, temp_local_files, local_folder, local_upload_folder):

        dataSynchronizer.update_remote_info(use_cache=False)

        statuses = dataSynchronizer.upload(
            local_upload_folder.replace(f'{local_folder}/', ""))

        for s in statuses:
            assert s in [HTTPStatus.NO_CONTENT, HTTPStatus.CREATED], s

    def test_download(self, dataSynchronizer, local_download_folder, local_folder):

        dataSynchronizer.update_remote_info(use_cache=False)
        statuses = dataSynchronizer.download(
            local_download_folder.replace(f'{local_folder}/', ""))

        assert len(os.listdir(local_download_folder)
                   ) == 5, "Number of downloaded files is not equal to expected"

        for s in statuses:
            assert s in [HTTPStatus.OK], s
