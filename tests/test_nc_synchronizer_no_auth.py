###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
import aiohttp

from oscar_nc.config_provider import ConfigProvider


class TestWithoutAuthClass:

    def test_remote_tree_update_deny(self, defaultDataSynchronizer):
        try:
            defaultDataSynchronizer.update_remote_info(use_cache=False)

            assert False, "Updating data without Auth successful"
        except Exception as e:
            assert isinstance(
                e, aiohttp.client_exceptions.ClientConnectorError)

    def test_local_tree(self, defaultDataSynchronizer, temp_local_files, local_folder):
        local_files = defaultDataSynchronizer.update_local_info()

        for f in temp_local_files:
            assert f.replace(f'{local_folder}/', "") in local_files

    def test_config_setup(self, local_folder, cfg_file):
        cfg_provider = ConfigProvider(cache_folder=local_folder)

        cfg_provider.set_config(os.path.abspath(cfg_file))
        new_config_path = cfg_provider.get_config_path()

        assert new_config_path == os.path.abspath(cfg_file)
